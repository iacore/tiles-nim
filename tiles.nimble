# Package

version       = "0.1.0"
author        = "Locria Cyber"
description   = "Typesetting rectangular text sections"
license       = "MIT"
srcDir        = "src"


# Dependencies

requires "nim >= 1.6"
