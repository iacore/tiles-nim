import macros
from strutils import count, `%`

proc strformatImpl(f: string; openChar, closeChar: char): NimNode =
  template missingCloseChar =
    error("invalid format string: missing closing character '" & closeChar & "'")

  if openChar == ':' or closeChar == ':':
    error "openChar and closeChar must not be ':'"
  var i = 0
  let res = genSym(nskVar, "fmtRes")
  result = newNimNode(nnkStmtListExpr)
  # XXX: https://github.com/nim-lang/Nim/issues/8405
  # When compiling with -d:useNimRtl, certain procs such as `count` from the strutils
  # module are not accessible at compile-time:
  let expectedGrowth = when defined(useNimRtl): 0 else: count(f, openChar) * 10
  result.add newVarStmt(res, newCall(bindSym"newStringOfCap",
                                     newLit(f.len + expectedGrowth)))
  var strlit = ""
  while i < f.len:
    if f[i] == openChar:
      inc i
      if f[i] == openChar:
        inc i
        strlit.add openChar
      else:
        if strlit.len > 0:
          result.add newCall(bindSym"add", res, newLit(strlit))
          strlit = ""

        var subexpr = ""
        var inParens = 0
        var inSingleQuotes = false
        var inDoubleQuotes = false
        template notEscaped:bool = f[i-1]!='\\'
        while i < f.len and f[i] != closeChar and (f[i] != ':' or inParens != 0):
          case f[i]
          of '\\':
            if i < f.len-1 and f[i+1] in {openChar,closeChar,':'}: inc i
          of '\'':
            if not inDoubleQuotes and notEscaped: inSingleQuotes = not inSingleQuotes
          of '\"':
            if notEscaped: inDoubleQuotes = not inDoubleQuotes
          of '(':
            if not (inSingleQuotes or inDoubleQuotes): inc inParens
          of ')':
            if not (inSingleQuotes or inDoubleQuotes): dec inParens
          of '=':
            let start = i
            inc i
            i += f.skipWhitespace(i)
            if i == f.len:
              missingCloseChar
            if f[i] == closeChar or f[i] == ':':
              result.add newCall(bindSym"add", res, newLit(subexpr & f[start ..< i]))
            else:
              subexpr.add f[start ..< i]
            continue
          else: discard
          subexpr.add f[i]
          inc i

        if i == f.len:
          missingCloseChar

        var x: NimNode
        try:
          x = parseExpr(subexpr)
        except ValueError as e:
          error("could not parse `$#` in `$#`.\n$#" % [subexpr, f, e.msg])
        let formatSym = bindSym("formatValue", brOpen)
        var options = ""
        if f[i] == ':':
          error("We don't support options yet via {expr:options}")
          # inc i
          # while i < f.len and f[i] != closeChar:
          #   options.add f[i]
          #   inc i
        if i == f.len:
          missingCloseChar
        if f[i] == closeChar:
          inc i
        # result.add newCall(formatSym, res, x, newLit(options))
        # TODO: add x
    elif f[i] == closeChar:
      if i<f.len-1 and f[i+1] == closeChar:
        strlit.add closeChar
        inc i, 2
      else:
        doAssert false, "invalid format string: '$1' instead of '$1$1'" % $closeChar
        inc i
    else:
      strlit.add f[i]
      inc i
  if strlit.len > 0:
    result.add newCall(bindSym"add", res, newLit(strlit))
  result.add res
  when defined(debugFmtDsl):
    echo repr result

macro t*(pattern: static string; openChar: static char, closeChar: static char): string =
  ## Interpolates `pattern` using symbols in scope.
  runnableExamples:
    let x = 7
    assert "var is {x * 2}".fmt == "var is 14"
    assert "var is {{x}}".fmt == "var is {x}" # escape via doubling
    const s = "foo: {x}"
    assert s.fmt == "foo: 7" # also works with const strings

    assert fmt"\n" == r"\n" # raw string literal
    assert "\n".fmt == "\n" # regular literal (likewise with `fmt("\n")` or `fmt "\n"`)
  runnableExamples:
    # custom `openChar`, `closeChar`
    let x = 7
    assert "<x>".fmt('<', '>') == "7"
    assert "<<<x>>>".fmt('<', '>') == "<7>"
    assert "`x`".fmt('`', '`') == "7"
  strformatImpl(pattern, openChar, closeChar)
