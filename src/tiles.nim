import std/[macros, strutils]

type Tile* = object

macro t*(pattern: static string; openChar: static char, closeChar: static char): Tile =
  quote: Tile()

func hjoin*(left, right: Tile): Tile =
  discard # todo

func `+`*(left, right: Tile): Tile =
  hjoin(left, right)

func vjoin*(up, down: Tile): Tile =
  discard # todo

func strip_padding*(s: string, linesep='\n'): Tile =
  var lines: seq[string] = s.split(linesep)
  # todo
  # strip top, bottom
  # strip left and right padding one char a time