# Tiles

Quote from https://github.com/sustrik/tiles

> **Tiles** is a simple ~~Python~~ Nim module meant to help with **code generation**.
> It provides a way to work with **rectangular areas of text** as atomic units.
> This is particularly important if proper **indentation** of the generated code
> is desired.
